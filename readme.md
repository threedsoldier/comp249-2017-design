COMP249 Assignment 1
====================

This directory contains the starter files for Assignment 1 for COMP249 in 2017.

Your task is to design the HTML and CSS for our new Psst Microblogging site. You
are provided with a basic page outline containing a number of messages. You must modify
the HTML and CSS to create a pleasing design.
